//
//  Car.swift
//  Best Car Offers
//
//  Created by Таир Адибек on 01.03.2024.
//

import Foundation

struct Car: Identifiable, Equatable {
    enum Status: String {
        case new = "new"
        case used = "used"
    }
    let id: String
    let model: String
    let brand: String
    let imagePath: String
    let status: Status
    let price: Double
    let mileage: Double
    let year: Int
    let numberOfOwners: Int
    let description: String
}

extension Car {
    static let mock: Self = Car(
        id: UUID().uuidString,
        model: "Camry",
        brand: "Toyota",
        imagePath: "https://motor.ru/imgs/2023/03/27/07/5850601/1d98eae9560f5256bcaf929a3ea01d2877d989cc.jpg",
        status: .new,
        price: 10000,
        mileage: 0,
        year: 2023,
        numberOfOwners: 0,
        description: "The Toyota Camry (/ˈkæmri/'; Japanese: トヨタ・カムリ Toyota Kamuri) is an automobile sold internationally by the Japanese auto manufacturer Toyota since 1982, spanning multiple generations."
    )
}

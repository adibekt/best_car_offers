//
//  CarsClient+Live.swift
//  Best Car Offers
//
//  Created by Таир Адибек on 01.03.2024.
//

import Foundation
import ComposableArchitecture

extension CarsClient {
    static let live: Self = {
        return CarsClient { 
            // imitate waiting for server response
            sleep(5)
            return [
                Car(
                    id: UUID().uuidString,
                    model: "Camry",
                    brand: "Toyota",
                    imagePath: "https://motor.ru/imgs/2023/03/27/07/5850601/1d98eae9560f5256bcaf929a3ea01d2877d989cc.jpg",
                    status: .new,
                    price: 10000,
                    mileage: 0,
                    year: 2023,
                    numberOfOwners: 0,
                    description: "The Toyota Camry (/ˈkæmri/'; Japanese: トヨタ・カムリ Toyota Kamuri) is an automobile sold internationally by the Japanese auto manufacturer Toyota since 1982, spanning multiple generations."
                ),
                Car(
                    id: UUID().uuidString,
                    model: "Land Cruiser Prado",
                    brand: "Toyota",
                    imagePath: "https://cdnstatic.rg.ru/crop1120x748/uploads/images/193/01/64/3559439.jpg",
                    status: .new,
                    price: 20000,
                    mileage: 0,
                    year: 2023,
                    numberOfOwners: 0,
                    description: "The Toyota Land Cruiser Prado (Japanese: トヨタ・ランドクルーザープラド, Hepburn: Toyota Rando-Kurūzā Purado) is a full-size four-wheel drive vehicle in the Land Cruiser range produced by the Japanese automaker Toyota as a \"light-duty\" variation in the range."
                ),
                Car(
                    id: UUID().uuidString,
                    model: "Niva",
                    brand: "Lada",
                    imagePath: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Lada_Niva_%28VAZ_2121%29.jpg/440px-Lada_Niva_%28VAZ_2121%29.jpg",
                    status: .used,
                    price: 20,
                    mileage: 160000,
                    year: 1995,
                    numberOfOwners: 29,
                    description: "Soviet and russian off-road vehicle"
                ),
            ]
        }
    }()
}
extension CarsClient: DependencyKey {
    static let liveValue = CarsClient.live
}

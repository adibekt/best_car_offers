//
//  CarsClient.swift
//  Best Car Offers
//
//  Created by Таир Адибек on 01.03.2024.
//

import Foundation
import ComposableArchitecture

struct CarsClient {
    var getCars: @Sendable () async throws -> [Car]
}

extension DependencyValues {
    var carsClient: CarsClient {
        get { self[CarsClient.self] }
        set { self[CarsClient.self] = newValue }
    }
}

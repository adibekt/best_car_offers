//
//  Best_Car_OffersApp.swift
//  Best Car Offers
//
//  Created by Таир Адибек on 01.03.2024.
//

import SwiftUI
import ComposableArchitecture

@main
struct Best_Car_OffersApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationStack {
                CartListView(
                    store: Store(
                        initialState: CarsListReducer.State()
                    ) {
                        CarsListReducer()._printChanges()
                    }
                )
            }
        }
    }
}

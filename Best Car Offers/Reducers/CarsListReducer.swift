//
//  CarsListReducer.swift
//  Best Car Offers
//
//  Created by Таир Адибек on 01.03.2024.
//

import Foundation
import ComposableArchitecture

@Reducer
struct CarsListReducer: Reducer {
    struct State: Equatable {
        var cars: [Car] = [] {
            didSet {
                filteredCars = cars
            }
        }
        var filteredCars: [Car] = []
        
        @PresentationState var destination: Destination.State?
    }
    
    enum Action: Equatable {
        case onAppear
        case carsReceived([Car])
        case onCarTap(Car)
        case destination(PresentationAction<Destination.Action>)
        case filter(String)
    }
    
    @Reducer
    struct Destination: Reducer {
        enum State: Equatable {
            case openCarPreview(CarPreviewReducer.State)
        }

        enum Action: Equatable {
            case openCarPreview(CarPreviewReducer.Action)
        }

        var body: some ReducerOf<Self> {
            Scope(state: /State.openCarPreview, action: /Action.openCarPreview) {
                CarPreviewReducer()
            }
        }
    }
    
    @Dependency(\.carsClient) var carsClient
    
    var body: some ReducerOf<Self> {
        Reduce<State, Action> { state, action in
            switch action {
            case let .onCarTap(car):
                state.destination = .openCarPreview(CarPreviewReducer.State(car: car))
                return .none

            case .onAppear:
                return .run { send in
                    try await send(.carsReceived(carsClient.getCars()))
                }
            case let .carsReceived(cars):
                state.cars = cars
                return .none
            case .destination(.presented(.openCarPreview(.delegate(.close)))):
                state.destination = nil
                return .none
            case .destination(_):
                return .none
            case let .filter(query):
                if query.isEmpty {
                    state.filteredCars = state.cars
                } else if query.count > 2 {
                    state.filteredCars = state.cars.filter({ $0.brand.contains(query) || $0.model.contains(query) })
                }
                return .none
            }
        }
        .ifLet(\.$destination, action: /Action.destination) {
            Destination()
        }
    }

}

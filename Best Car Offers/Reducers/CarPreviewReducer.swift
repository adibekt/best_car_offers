//
//  CarPreviewReducer.swift
//  Best Car Offers
//
//  Created by Таир Адибек on 02.03.2024.
//

import Foundation
import ComposableArchitecture

@Reducer
struct CarPreviewReducer: Reducer {
    struct State: Equatable {
        let car: Car
    }
    
    enum Action: Equatable {
        case onCloseTap
        case delegate(Delegate)

        enum Delegate: Equatable {
            case close
        }
    }
    
    var body: some ReducerOf<Self> {
        Reduce<State, Action> { _, action in
            switch action {
            case .onCloseTap:
                return .send(.delegate(.close))
            case .delegate(_):
                return .none
            }
        }
    }
}

//
//  CarItemCell.swift
//  Best Car Offers
//
//  Created by Таир Адибек on 02.03.2024.
//

import SwiftUI

struct CarItemCell: View {
    
    private enum Constants {
        static let imageSize: CGFloat = 60
        static let imageCornerRadius: CGFloat = 12
    }
    
    let car: Car
    
    var body: some View {
        HStack {
            AsyncImage(
                url: URL(string: car.imagePath),
                content: {
                    image in
                    image.resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(
                            maxWidth: Constants.imageSize,
                            maxHeight: Constants.imageSize
                        )
                },
                placeholder: {
                    ProgressView()
                }
            )
                .frame(
                    width: Constants.imageSize,
                    height: Constants.imageSize
                )
                .padding()
                .clipShape(
                    RoundedRectangle(
                        cornerRadius: Constants.imageCornerRadius
                    )
                )
            
            VStack(alignment: .leading) {
                Text(car.brand + ": " + car.model)
                    .font(.headline)
                Text(car.description)
                    .lineLimit(3)
                    .font(.subheadline)
            }
        }
    }
}

#Preview {
    CarItemCell(car: .mock)
}

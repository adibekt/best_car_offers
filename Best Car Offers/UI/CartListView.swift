//
//  CartListView.swift
//  Best Car Offers
//
//  Created by Таир Адибек on 01.03.2024.
//

import SwiftUI
import ComposableArchitecture

struct CartListView: View {
    
    let store: StoreOf<CarsListReducer>
    @State var searchQuery: String = ""
    var body: some View {
        
        WithViewStore(self.store, observe: { $0 }) { viewStore in
            if viewStore.cars.isEmpty {
                ProgressView()
            }
            NavigationStack{
                List {
                    ForEach(viewStore.filteredCars) { car in
                        CarItemCell(car: car)
                            .onTapGesture {
                                viewStore.send(.onCarTap(car))
                            }
                    }
                }
                .onAppear {
                    viewStore.send(.onAppear)
                }
                .sheet(
                    store: store.scope(state: \.$destination, action: \.destination),
                    state: /CarsListReducer.Destination.State.openCarPreview,
                    action: CarsListReducer.Destination.Action.openCarPreview
                ) { store in
                    CarPreviewView(store: store)
                }
                .searchable(text: $searchQuery)
                .onChange(of: searchQuery) { oldValue, newValue in
                    viewStore.send(.filter(newValue))
                }
            }
        }
        
        
    }
}

#Preview {
    CartListView(
        store: Store(
            initialState: CarsListReducer.State(),
            reducer: { 
                CarsListReducer()._printChanges()
            }
        )
    )
}

//
//  CarPreviewView.swift
//  Best Car Offers
//
//  Created by Таир Адибек on 01.03.2024.
//

import SwiftUI
import ComposableArchitecture

struct CarPreviewView: View {
    private enum Constants {
        static let imageSize: CGFloat = 60
        static let imageCornerRadius: CGFloat = 12
    }
    
    let store: StoreOf<CarPreviewReducer>
    
    var body: some View {
        WithViewStore(self.store, observe: { $0 }) { viewStore in
            
            ScrollView {
                    VStack(alignment: .leading) {
                        topBarView {
                            viewStore.send(.onCloseTap)
                        }  
                        AsyncImage(
                            url: URL(
                                string: viewStore.car.imagePath
                            ),
                            content: {
                                image in
                                image.resizable()
                                    .aspectRatio(contentMode: .fit)
                                
                            },
                            placeholder: {
                                ProgressView()
                            }
                        )
                        .padding()

                        infoView(
                            title: "Brand",
                            value: viewStore.car.brand
                        )
                        infoView(
                            title: "Model",
                            value: viewStore.car.model
                        )
                        infoView(
                            title: "Year of construction",
                            value: "\(viewStore.car.year)"
                        )
                        infoView(
                            title: "Number of owners",
                            value: viewStore.car.numberOfOwners.formatted()
                        )
                        infoView(
                            title: "Mileage",
                            value: viewStore.car.mileage.formatted()
                        )
                        infoView(
                            title: "Status",
                            value: viewStore.car.status.rawValue
                        )
                        infoView(
                            title: "Description",
                            value: viewStore.car.description
                        )
                        Spacer()
                    }
                }
        }
    }
    
    func topBarView(
        onCloseTap: @escaping () -> Void
    ) -> some View {
        HStack {
            Button(action: {
                onCloseTap()
            }, label: {
                Image(systemName: "xmark")
                    .padding()
            })
        }
    }
    
    func infoView(
        title: String,
        value: String
    ) -> some View {
        HStack(alignment: .top) {
            Text(title + ":")
                .font(.system(size: 14, weight: .bold))
            Text(value)
                .font(.system(size: 14, weight: .regular))
                .lineLimit(nil)
        }
        .padding(.horizontal)
    }
}

#Preview {
    CarPreviewView(
        store: Store(
            initialState: CarPreviewReducer.State(
                car: .mock
            ),
            reducer: { 
                CarPreviewReducer()._printChanges()
            }
        )
    )
}
